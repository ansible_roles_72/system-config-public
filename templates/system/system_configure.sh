#!/usr/bin/env bash

system_packages_install(){
	apt-get update
	echo iptables-persistent iptables-persistent/autosave_v4 boolean true |  debconf-set-selections
	echo iptables-persistent iptables-persistent/autosave_v6 boolean true |  debconf-set-selections
	DEBIAN_FRONTEND=noninteractive apt-get install rsync git mc iperf3 tcpdump traceroute debconf-utils make iotop htop bash-completion pigz iptables-persistent cron nano  dnsutils iptables-persistent -y
}

system_security(){
	grep -q 'portmap: 10.0.0.0/8' /etc/hosts.allow  && echo "already done"  || echo "portmap: 10.0.0.0/8" >> /etc/hosts.allow
	grep -q 'rpcbind: 10.0.0.0/8' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 10.0.0.0/8" >> /etc/hosts.allow
	grep -q 'portmap: 192.168.0.0/16' /etc/hosts.allow  && echo "already done"  || echo "portmap: 192.168.0.0/16" >> /etc/hosts.allow
	grep -q 'rpcbind: 192.168.0.0/16' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 192.168.0.0/16" >> /etc/hosts.allow
	grep -q 'portmap: 172.16.0.0/16' /etc/hosts.allow  && echo "already done"  || echo "portmap: 172.16.0.0/16" >> /etc/hosts.allow
	grep -q 'rpcbind: 172.16.0.0/16' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 172.16.0.0/16" >> /etc/hosts.allow
	grep -q 'portmap: 127.0.0.1/32' /etc/hosts.allow  && echo "already done"  || echo "portmap: 127.0.0.1/32" >> /etc/hosts.allow
	grep -q 'rpcbind: 172.0.0.1/32' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 172.0.0.1/32" >> /etc/hosts.allow
	grep -q 'rpcbind: 172.22.0.0/16' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 172.22.0.0/16" >> /etc/hosts.allow
	grep -q 'portmap: ALL' /etc/hosts.deny && echo "already done" || echo "portmap: ALL" >> /etc/hosts.deny
	grep -q 'rpcbind: ALL' /etc/hosts.deny && echo "already done" || echo "rpcbind: ALL" >> /etc/hosts.deny
	cat /etc/hosts.allow && cat /etc/hosts.deny
	echo iptables-persistent iptables-persistent/autosave_v4 boolean true |  debconf-set-selections
	echo iptables-persistent iptables-persistent/autosave_v6 boolean true |  debconf-set-selections
	apt-get -y install iptables-persistent
	iptables -A INPUT -p udp -s 10.0.0.0/8 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 172.16.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 172.22.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 127.0.0.1 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 192.168.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 10.0.0.0/8 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 172.16.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 172.22.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 127.0.0.1 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 192.168.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp --dport 111 -j DROP
	iptables -A INPUT -p udp --dport 111 -j DROP
	netfilter-persistent save
	netfilter-persistent start

}




system_dns_server_set(){
	DEBIAN_FRONTEND=noninteractive apt install resolvconf -y
	systemctl disable systemd-resolved
	systemctl stop systemd-resolved
	systemctl enable resolvconf
	rm /etc/resolvconf/resolv.conf.d/head /etc/resolv.conf
	echo "nameserver {{ system_dns_server | default('8.8.8.8') }}" >  /etc/resolvconf/resolv.conf.d/head
	echo "nameserver {{ system_dns_server | default('8.8.8.8') }}" > /etc/resolv.conf 
	systemctl restart resolvconf
	#ln -sf /run/resolvconf/ /run/systemd/resolve
}

system_swap_add() {

SWAP_FILE="/swap_extended.img"

if [[ ! -f "${SWAP_FILE}" ]];then
	fallocate -l 4G ${SWAP_FILE};
	chown root:root ${SWAP_FILE};
	chmod 0600 ${SWAP_FILE};
	mkswap ${SWAP_FILE};
	swapon "${SWAP_FILE}"  
	grep -q "${SWAP_FILE}"  /etc/fstab || echo "${SWAP_FILE} node swap sw 0 0 "  >> /etc/fstab
else
	echo "${SWAP_FILE} already exist"
fi
}


main(){
	#system_security
	system_packages_install
	system_dns_server_set
	#system_swap_add
	

}
main