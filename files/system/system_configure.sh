#!/usr/bin/env bash

system_security(){
	grep -q 'portmap: 10.0.0.0/8' /etc/hosts.allow  && echo "already done"  || echo "portmap: 10.0.0.0/8" >> /etc/hosts.allow
	grep -q 'rpcbind: 10.0.0.0/8' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 10.0.0.0/8" >> /etc/hosts.allow
	grep -q 'portmap: 192.168.0.0/16' /etc/hosts.allow  && echo "already done"  || echo "portmap: 192.168.0.0/16" >> /etc/hosts.allow
	grep -q 'rpcbind: 192.168.0.0/16' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 192.168.0.0/16" >> /etc/hosts.allow
	grep -q 'portmap: 172.16.0.0/12' /etc/hosts.allow  && echo "already done"  || echo "portmap: 172.16.0.0/12" >> /etc/hosts.allow
	grep -q 'rpcbind: 172.16.0.0/12' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 172.16.0.0/12" >> /etc/hosts.allow
	grep -q 'portmap: 127.0.0.1/32' /etc/hosts.allow  && echo "already done"  || echo "portmap: 127.0.0.1/32" >> /etc/hosts.allow
	grep -q 'rpcbind: 172.0.0.1/32' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 172.0.0.1/32" >> /etc/hosts.allow
	grep -q 'rpcbind: 172.16.0.0/12' /etc/hosts.allow  && echo "already done" || echo "rpcbind: 172.16.0.0/12" >> /etc/hosts.allow
	grep -q 'portmap: ALL' /etc/hosts.deny && echo "already done" || echo "portmap: ALL" >> /etc/hosts.deny
	grep -q 'rpcbind: ALL' /etc/hosts.deny && echo "already done" || echo "rpcbind: ALL" >> /etc/hosts.deny
	cat /etc/hosts.allow && cat /etc/hosts.deny
	echo iptables-persistent iptables-persistent/autosave_v4 boolean true |  debconf-set-selections
	echo iptables-persistent iptables-persistent/autosave_v6 boolean true |  debconf-set-selections
	apt-get -y install iptables-persistent
	iptables -A INPUT -p udp -s 10.0.0.0/8 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 172.16.0.0/12 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 172.16.0.0/12 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 127.0.0.1 --dport 111 -j ACCEPT
	iptables -A INPUT -p udp -s 192.168.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 10.0.0.0/8 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 172.16.0.0/12 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 172.16.0.0/12 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 127.0.0.1 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp -s 192.168.0.0/16 --dport 111 -j ACCEPT
	iptables -A INPUT -p tcp --dport 111 -j DROP
	iptables -A INPUT -p udp --dport 111 -j DROP
	netfilter-persistent save
	netfilter-persistent start

}

main(){
	system_security
	

	

}
main