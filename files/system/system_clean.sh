#!/usr/bin/env bash
#./system_clean.sh --action=full
for i in "$@"
do
case $i in
    --action=*) ##
    ACTION="${i#*=}"
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done

export arg_nice="nice -n19 ionice -c2 -n7"
export arg_time="4"


if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 $1
fi

action_logrotate(){
    ${arg_nice} logrotate /etc/logrotate.conf 2>/dev/null || echo "skip logrotate"
    $(which supervisorctl) restart rsyslogd 1>/dev/null || (echo "restart rsyslog via systemctl" && systemctl restart rsyslog)
}


action_docker_images_clean(){
    docker images -q -a | xargs --no-run-if-empty ${arg_nice} docker rmi -f 2>/dev/null
    docker system prune -af
}


action_docker_logs_clean(){
    echo "Exec ${FUNCTNAME}"
    PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin"
    docker_path=$(docker info 2>/dev/null | grep 'Docker Root Dir' | awk '{ print $NF }')
    ${arg_nice} find "${docker_path}"/containers -type f -name '*-json.log' | xargs -r0 -n10 -P3 bash -c 'echo $@; du -sh --block-size=1M  $@; chmod -t $@;truncate -s 0 $@; du -sh $@;' bash
    ${arg_nice} find "${docker_path}"/containers -type f -name '*-json.log' | xargs -r0 -n10 -P3 bash -c 'du -sh --block-size=1M  $@;' bash
}

k3s_clean() {
    if [[ -d "/var/lib/rancher/k3s/server/logs" ]];then
        ${arg_nice} find /var/lib/rancher/k3s/server/logs -type f -name 'audit-*' -mmin +120 | xargs rm -v -f || (echo "Noting to do")
    fi

    if [[ -d "/var/log/pods" ]];then
        ${arg_nice} find /var/log/pods  -mindepth 1 -maxdepth 5  -type f  -mmin +600 | xargs rm -v -f || (echo "Noting to do")
    fi

}

containerd_prune_images () {
    if ! command -v crictl &> /dev/null
    then
        echo "info. crictl could not be found. Skip crictl rmi --prune "
    else
        echo "containerd found"
        crictl rmi --prune
    fi
}

fluent_logs_clean(){
    if docker ps | grep flu | grep forwarder
    then
        nice --adjustment=19 ionice --class 3 /usr/bin/find "/fluentdlogs" -mindepth 1 -maxdepth 2 -type f  -iname '*log*' -mmin +1400 | xargs rm -v -f || (echo "Noting to do")
    else
        echo "fluentd-forwarder not exit. Skip clean logs"
    fi
}

misc_run(){
    if [[ ! -d "/var/log/supervisor" ]];then mkdir -p /var/log/supervisor;fi
    chmod 755 /var/log/;
    chown root:root /var/log/
}


main(){
    misc_run
    if [[ "${ACTION}" == "logrotate" ]];then action_logrotate; fi
    if [[ "${ACTION}" == "docker_images_clean" ]];then action_docker_images_clean;containerd_prune_images; fi
    if [[ "${ACTION}" == "docker_logs_clean" ]];then action_docker_logs_clean; fi
    if [[ "${ACTION}" == "full" ]];then
        action_docker_logs_clean
        action_docker_images_clean
        containerd_prune_images
        action_logrotate
        k3s_clean
     fi 

    
}

main
